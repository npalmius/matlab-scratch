function random_classification()
% Copyright (c) 2015, Nick Palmius (University of Oxford)
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%
% 1. Redistributions of source code must retain the above copyright
%    notice, this list of conditions and the following disclaimer.
%
% 2. Redistributions in binary form must reproduce the above copyright
%    notice, this list of conditions and the following disclaimer in the
%    documentation and/or other materials provided with the distribution.
%
% 3. Neither the name of the University of Oxford nor the names of its
%    contributors may be used to endorse or promote products derived
%    from this software without specific prior written permission.
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
% "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
% LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
% A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
% HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
% SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
% LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
% DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
% THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
% (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

% Contact: npalmius@googlemail.com
% Originally written by Nick Palmius, 13-Jan-2015
    close all;
    
    classification_results_train = NaN(100, 20);
    classification_results_test = NaN(100, 20);
    
    class = round(rand(100, 1));
    
    for i = [1:10 12:2:20 25:5:50 60:10:100]
        i
        for j = 1:20
            features_train = rand(100,i);
            features_test = rand(100,i);
            
            try
              svm = svmtrain(features_train, class);
            catch
                continue;
            end
            
            pred_train = svmclassify(svm, features_train);
            pred_test = svmclassify(svm, features_test);
            
            cp_train = classperf(class, pred_train);
            cp_test = classperf(class, pred_test);
            
            classification_results_train(i, j) = cp_train.CorrectRate;
            classification_results_test(i, j) = cp_test.CorrectRate;
        end
    end
    
    figure;
    boxplot(classification_results_train', 'plotstyle', 'compact', 'colors', 'r');
    set(gca,'XTickLabel',{' '})
    hold on;
    boxplot(classification_results_test', 'plotstyle', 'compact', 'colors', 'g');
    set(gca,'XTickLabel',{' '})
    hold off;
    
    set(gca, 'ylim', [0.275, 1.025]);
    set(gca, 'xlim', [-0.5, 100.5]);
    
    boxes = findobj(gca,'Tag','Box');
    legend(boxes([101, 1]), 'Training instances', 'Test instances', 'location', 'NorthWest')
    
    set(gca,'XTick',0:10:100)
    set(gca,'XTickLabel',0:10:100)
    
    title('SVM training and test accuracy on random features', 'fontsize', 20);
    
    xlabel('Number of features', 'fontsize', 20);
    ylabel('Classification accuracy', 'fontsize', 20);
    
    set(gca, 'fontsize', 20)
    
    xlabh = get(gca,'XLabel');
    set(xlabh,'Position',get(xlabh,'Position') - [0 10 0]);
    copyobj(xlabh, gca);
    delete(xlabh);
    
    save_figure('random_classification', 'RandomClassification');
end
